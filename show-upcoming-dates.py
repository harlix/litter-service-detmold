
import sys
import json
import argparse
import datetime

from lsd import get_upcoming_dates_for_street

def make_printable_table_row(*fields, width=32):
    fmtStr = ''
    for f in fields:
        fmtStr += '{:' + str(width) + 's}'
    return fmtStr.format(*fields)

def make_german_formatted_date(d):
    prosa = ''
    if d['daysRemaining'] == 0:
        prosa = 'heute'
    elif d['daysRemaining'] == 1:
        prosa = 'morgen'
    elif d['daysRemaining'] == 2:
        prosa = 'übermorgen'
    else:
        prosa = 'in {} Tagen'.format(d['daysRemaining'])

    return '{} ({})'.format(d['date'].strftime('%d.%m.%Y'), prosa)

def print_garbo_dates_as_german_text_table(streetName, dates):
    print('')
    print( make_printable_table_row('Datum:', datetime.datetime.now().date().strftime('%d.%m.%Y'), width=16) )
    print( make_printable_table_row('Straße:', streetName, width=16) )
    print( make_printable_table_row('Ereignisse:', str(len(dates)), width=16) )
    print('')
    print( make_printable_table_row('DATUM', 'EREIGNIS') )
    print( make_printable_table_row('-----', '--------') )
    for d in dates:
        print( make_printable_table_row( make_german_formatted_date(d), d['description'] ) )
 
    print('')

def print_garbo_dates_as_compact_german_list(dates):
    for d in dates:
        print( make_german_formatted_date(d) )
        print( d['description'] )
        if len(dates) > 1:
            print('')

def print_garbo_dates_as_json(streetName, dates, indent=4):

    ''' 
    See https://stackoverflow.com/questions/11875770/how-to-overcome-datetime-datetime-not-json-serializable
    '''
    def default(o):
        if isinstance(o, (datetime.date, datetime.datetime)):
            return o.isoformat()

    r = {
        'street': streetName,
        'upcomingDates': dates
    }

    print(json.dumps(r, indent=indent, default=default))

if __name__ == '__main__':

    parser = argparse.ArgumentParser()
    
    parser.add_argument('street', type=str, help='the name of the street in Detmold (required)')
    parser.add_argument('-n', '--number', type=int, default=1, help='number of upcoming garbo dates (default: 1)')
    parser.add_argument('--json', action='store_true', help='output as json (default is table)')
    parser.add_argument('--list', action='store_true', help='output as compact list (default is table)')

    args = parser.parse_args()

    streetName = args.street.title()
    numEvents = args.number
 
    try:

        if args.json and args.list:
            raise RuntimeError("invalid options: '--json' is not allowed together with '--list'")

        nextGarboDates = get_upcoming_dates_for_street(streetName, numEvents)
        if args.json:
            print_garbo_dates_as_json(streetName, nextGarboDates)

        elif args.list:
            print_garbo_dates_as_compact_german_list(nextGarboDates)

        else:
            print_garbo_dates_as_german_text_table(streetName, nextGarboDates)

    except Exception as ex:
       print(ex, file=sys.stderr)
       sys.exit(1)
