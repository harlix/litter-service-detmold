from html.parser import HTMLParser

class IndexHTMLParser(HTMLParser):

    def __init__(self):
        super().__init__()
        self._inScriptTag = False
        self._embeddedScripts = []

    ''' Inherited methods '''
    def handle_starttag(self, tag, attrs):
        if tag == 'script':
            self._inScriptTag = True

    def handle_endtag(self, tag):
        if tag == 'script':
            self._inScriptTag = False

    def handle_data(self, data):
        if self._inScriptTag == True:
            data = data.strip()
            if len(data) > 0:
                self._embeddedScripts.append(data)

    ''' Accessor for extracted data '''
    def getEmbeddedScripts(self):
        return self._embeddedScripts
