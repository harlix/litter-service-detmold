
from datetime import datetime

from .icalparser import ICalParser

class ICalEventParser(ICalParser):

    def __init__(self):
        super().__init__()
        self._dates = []
        self._currentDate = None

    def get_event_objects(self):
        return self._dates

    def handle_starttag(self, tag):
        if tag == 'VEVENT':
            self._currentDate = {
                'start': None,
                'end': None,
                'description': None,
                'location': None,
            }

    def handle_endtag(self, tag):
        if tag == 'VEVENT':
            self._dates.append(self._currentDate)
            self._currentDate = None

    def handle_property(self, key, attrs, value):
        if self._currentDate is None:
            return

        if key == 'DTSTART':
            if len(attrs) == 1 and attrs[0] == 'VALUE=DATE':
                try: 
                    self._currentDate['start'] = datetime.strptime(value, '%Y%m%d').date()
                except:
                    raise RuntimeError('failed to parse start date property: {}'.format(value))

        elif key == 'DTEND':
            if len(attrs) == 1 and attrs[0] == 'VALUE=DATE':
                try: 
                    self._currentDate['end'] = datetime.strptime(value, '%Y%m%d').date()
                except:
                    raise RuntimeError('failed to parse end date property: {}'.format(value))

        elif key == 'LOCATION':
            self._currentDate['location'] = value

        elif key == 'DESCRIPTION':
            self._currentDate['description'] = value
