
import re
import json
from datetime import datetime
from http.client import HTTPConnection, HTTPSConnection

from .htmlparser import IndexHTMLParser
from .icaleventparser import ICalEventParser

''' Detmold city waste calendar API paths '''
ABFUHRKALENDER_BASE_URL = 'https://abfuhrkalender.detmold.de'
ABFUHRKALENDER_ICSMAKER_PATH = '/icsmaker.php?strid={}&year={}'


def _get_plain_text_resource(baseUrl, path='/'):

    # print('http request: GET {}{}'.format(baseUrl, path))

    urlParts = baseUrl.split('://')
    if len(urlParts) != 2 or urlParts[0] not in ('http', 'https'):
        raise RuntimeError('illegal base url: {}'.format(baseUrl))

    if urlParts[0] == 'https':
        conn = HTTPSConnection(urlParts[1], 443)
    else:
        conn = HTTPConnection(urlParts[1], 80)

    conn.request('GET', path)
    resp = conn.getresponse()

    if resp.status != 200:
        raise RuntimeError('http error: GET {}{}: server returned status {} {}'
            .format(baseUrl, path, resp.status, resp.reason))

    return resp.read()
    
def _find_streetindex_by_streetname_in_text(text, streetName):

    result = None

    for line in text.split('\n'):
        line = line.strip()
        ''' 
        The line we are searching for looks like so:

        strasse[383] = {"name":"Hasenpfad","tag":"2"};

        There is a JSON object assigned to an array element. The most reliable 
        way to pick the correct line is to split the assignment, parse the json and compare 
        the contained street name. If the JSON parsing fails, this is probably another line 
        somewhere else in the code containing the same street name, which is not what we want.
        '''
        if line[:7] != 'strasse':
            continue

        lineParts = line.split('=')
        if len(lineParts) != 2:
            continue

        rhs = lineParts[1].strip().replace(';', '')
        match = False
        try:
            jsonObject = json.loads(rhs)
            '''
            Some street names are abbreviated with "str.", expand them for consitency. 
            Additionally, lowercase all names and replace "ß" with "ss", so the search
            algorithm is case-insensitive and can handle both "ß" and "ss" writing styles,
            e.g. "Meierstraße" as well as "MEIERSTRASSE" or even "meierSTRaße".
            '''
            a = streetName.lower().replace('ß', 'ss')
            b = jsonObject['name'].lower().replace('ß', 'ss').replace('str.', 'strasse')
            
            if a == b:
                match = True

        except:
            ''' 
            Malformed JSON or no JSON encoded data at all, continue with next line 
            '''
            continue

        if not match:
            continue

        '''
        Extraxt the street index from the lefthand side of the assignment (e.g. strasse[383]).
        The street index is the same as the array index.
        '''
        lhs = lineParts[0].replace(' ', '').strip()
        m = re.search('\\[([0-9]+)\\]', lhs)
        if m is None:
            continue

        return int(m.group(1))
        
    return None


def _get_streetindex_by_streetname(streetName):
    ''' 
    Fetch the index.html page, which contains all street names along
    with their indices in an embedded javascript object. Extract the
    javascript and fiddle out the line containing the desired data.
    '''
    indexHtml = _get_plain_text_resource(ABFUHRKALENDER_BASE_URL)
    indexParser = IndexHTMLParser()
    indexParser.feed(indexHtml.decode('utf-8'))

    ''' 
    Since there might be multiple embedded scripts, we have to iterate 
    over them until we find the desired data.
    '''
    scripts = indexParser.getEmbeddedScripts()
    for script in scripts:
        streetIndex = _find_streetindex_by_streetname_in_text(script, streetName)
        if streetIndex is not None:
            return streetIndex

    return None

def _get_ical_by_street_index(streetIndex, year):
    path = ABFUHRKALENDER_ICSMAKER_PATH.format(streetIndex, year)
    return _get_plain_text_resource(ABFUHRKALENDER_BASE_URL, path)

def _get_bin_colors_from_description(description):
    '''
    Description is like: "Müllabfuhr: Gelber Sack, Papier"
    This method parses the string and generates an array of waste type flags (bin colors):
    "Bioabfall"     --> "GN"
    "Restmüll"      --> "GR"
    "Gelber Sack"   --> "YE"
    "Papier"        --> "BL"
    '''
    flags = []
    if re.search('Bioabfall', description) is not None:
        flags.append('GN')
    if re.search('Restmüll', description) is not None:
        flags.append('GR')
    if re.search('Gelber Sack', description) is not None:
        flags.append('YE')
    if re.search('Papier', description) is not None:
        flags.append('BL')
    return flags

def _get_remaining_dates_for_year(streetIndex, year):
    iCalData = _get_ical_by_street_index(streetIndex, year)

    iCalParser = ICalEventParser()
    iCalParser.feed(iCalData.decode('utf-8'))

    dates = iCalParser.get_event_objects()
    remainingDates = []
    today = datetime.now().date()

    for d in dates:
        if d['start'] is None:
            raise RuntimeError('error: invalid calendar data without start date')
        if d['description'] is None:
            raise RuntimeError('error: invalid calendar data without description')

        if d['start'] >= datetime.now().date():
            binColors = _get_bin_colors_from_description(d['description'])
            remainingDates.append({
                'date': d['start'],
                'description': d['description'],
                'daysRemaining': (d['start'] - today).days,
                'binColors': binColors
            })
    
    return remainingDates

'''
Public API
'''
def get_upcoming_dates_for_street(streetName, count):
    upcomingDates = []
    year = datetime.now().year

    streetIndex = _get_streetindex_by_streetname(streetName)
    if streetIndex is None:
        raise RuntimeError("Street '{}' not found in Detmold City".format(streetName))

    # print('Street index for {} is: {}'.format(streetName, streetIndex)) 
    remaining = count
    while remaining > 0:
        remainingDates = _get_remaining_dates_for_year(streetIndex, year)
        if len(remainingDates) == 0:
            raise RuntimeError('error: too much events requested: no more dates for the year {}'.format(year))

        upcomingDates += remainingDates[:remaining]
        remaining = count - len(upcomingDates)
        year += 1

    return upcomingDates