
class ICalParser:

    def __init__(self):
        self._inVCALENDAR = False
        self._inVEVENT = False
        self._inVALARM = False

    def handle_starttag(self, tag):
        print('BEGIN:{}'.format(tag))

    def handle_endtag(self, tag):
        print('END:{}'.format(tag))

    def handle_property(self, key, attrs, value):
        print('PROPERTY:', key, attrs, value)
    
    def feed(self, data):
        n = 0
        for line in data.splitlines():
            n += 1
            line = line.strip()

            if len(line) == 0:
                continue

            if line[:6] == 'BEGIN:':
                try:
                    self._handle_starttag_intern(line[6:])
                except RuntimeError as ex:
                    raise RuntimeError('ical parser error: in line {}: {}'.format(n, str(ex)))
                continue

            if line[:4] == 'END:':
                try:
                    self._handle_endtag_intern(line[4:])
                except RuntimeError as ex:
                    raise RuntimeError('ical parser error: in line {}: {}'.format(n, str(ex)))
                continue
    
            kv = line.split(':')
            if len(kv) > 1:
                key = kv[0]
                value = line[(len(key)+1):]
                keyParts = key.split(';')
                try:
                    if len(keyParts) > 1:
                        self.handle_property(keyParts[0], keyParts[1:], value)
                    else:
                        self.handle_property(keyParts[0], [], value)
                except Exception as ex:
                    raise RuntimeError('ical parser error: in line {}: {}'.format(n, str(ex)))

    def _handle_starttag_intern(self, tag):
        if tag == 'VCALENDAR':
            self._inVCALENDAR = True
        
        elif tag == 'VEVENT':
            if not self._inVCALENDAR:
                raise RuntimeError('BEGIN:VEVENT outside VCALENDAR')
            self._inVEVENT = True

        elif tag == 'VALARM':
            if not self._inVEVENT:
                raise RuntimeError('BEGIN:VALARM outside VEVENT')
            self._inVALARM = True

        self.handle_starttag(tag)

    def _handle_endtag_intern(self, tag):
        if tag == 'VCALENDAR':
            self._inVCALENDAR = False
        
        elif tag == 'VEVENT':
            if not self._inVCALENDAR:
                raise RuntimeError('END:VEVENT outside VCALENDAR')
            self._inVEVENT = False

        elif tag == 'VALARM':
            if not self._inVEVENT:
                raise RuntimeError('END:VALARM outside VEVENT')
            self._inVALARM = False

        self.handle_endtag(tag)
