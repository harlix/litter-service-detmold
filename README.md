
# Tool for querying the garbage collection API of Detmold, Germany

## Important advice

The litter service calendar of Detmold does not provide a well-designed, 
stable API which is intended to be queried programmatically. There is in fact
only a simple static website which contains all the content of the litter 
service database as embedded script objects and does not query any backend API
at all. However, there is also a simple iCal API which can be used to get the 
calendar data for a particular street, but this API requires a street id instead 
of a street name. The street id has to be extracted from the static website's 
embedded scripts first, which is not be reliable, since it depends on how the 
emdedded javascript is formatted. All this was investigated by try-and-error,
there is no documentation at all.

**Because of the conditions noted above and the fact that the city of Detmold**
**does not explicitly considers to use their web-based litter service calendar**
**as a web API, this project must not be used in production! It is intended** 
**to serve as a basis for own personal experiments only.**

## Python version

The project works with Python 3 (developed with Python 3.6, any newer version 
should also work). Python 2 is not supported!

## Usage

```
user@litter-service-detmold$ python show-upcoming-dates.py -h
usage: litter-service-detmold [-h] [-n NUMBER] [--json] street

positional arguments:
  street                the name of the street in Detmold (required)

optional arguments:
  -h, --help            show this help message and exit
  -n NUMBER, --number NUMBER
                        number of upcoming garbo dates (default: 1)
  --json                output as json (default is pretty human readable
                        german text)
```

## Example output (pretty German text, showing next 5 upcoming dates)

```
user@litter-service-detmold$ python show-upcoming-dates.py Seminarstraße -n5

Datum:          28.11.2019      
Straße:         Seminarstraße   
Ereignisse:     5               

DATUM                           EREIGNIS                        
-----                           --------                        
28.11.2019 (heute)              Müllabfuhr: Bioabfall           
05.12.2019 (in 7 Tagen)         Müllabfuhr: Gelber Sack, Papier 
12.12.2019 (in 14 Tagen)        Müllabfuhr: Bioabfall           
19.12.2019 (in 21 Tagen)        Müllabfuhr: Gelber Sack, Restmüll
27.12.2019 (in 29 Tagen)        Müllabfuhr: Bioabfall           


```

## Example output (with --json option)
```
user@litter-service-detmold$ python show-upcoming-dates.py "krohnstraße" -n4 --json
{
    "street": "Krohnstra\u00dfe",
    "upcomingDates": [
        {
            "date": "2019-11-28",
            "description": "M\u00fcllabfuhr: Bioabfall",
            "daysRemaining": 1,
            "binColors": [
                "GN"
            ]
        },
        {
            "date": "2019-12-05",
            "description": "M\u00fcllabfuhr: Gelber Sack, Papier",
            "daysRemaining": 8,
            "binColors": [
                "YE",
                "BL"
            ]
        },
        {
            "date": "2019-12-12",
            "description": "M\u00fcllabfuhr: Bioabfall",
            "daysRemaining": 15,
            "binColors": [
                "GN"
            ]
        },
        {
            "date": "2019-12-19",
            "description": "M\u00fcllabfuhr: Gelber Sack, Restm\u00fcll",
            "daysRemaining": 22,
            "binColors": [
                "GR",
                "YE"
            ]
        }
    ]
}
```